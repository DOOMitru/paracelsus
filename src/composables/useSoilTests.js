import { ref, computed } from 'vue'
import { uid, Dialog } from 'quasar'
import AddSoilTestDialog from 'components/AddSoilTestDialog.vue'

const sortFn = (a, b) => b.DateCreated - a.DateCreated
const latestFilterFn = (t, index) => index < 10
const archivedFilterFn = t => t.Archived
const currentFilterFn = t => !t.Archived

const allSoilTests = ref([])

const soilTests = computed(() => allSoilTests.value.filter(currentFilterFn))
const archivedSoilTests = computed(() => allSoilTests.value.filter(archivedFilterFn))
const latestTests = computed(() => soilTests.value.sort(sortFn).filter(latestFilterFn))

export const columns = [
	{
		name: 'name',
		field: 'name',
		label: 'Name',
		align: 'left',
	},
	{
		name: 'description',
		field: 'description',
		label: 'Description',
		align: 'left',
	},
	{
		name: 'actions',
		field: 'actions',
		label: '',
		align: 'right',
	},
]

export default function useSoilTests() {
	const addNewSoilTest = soilTest => {
		allSoilTests.value.push({
			...soilTest,
			DateCreated: +new Date(),
			Archived: false,
			DateArchived: null,
		})
	}

	const deleteSoilTest = soilTestId => {
		allSoilTests.value = allSoilTests.value.filter(t => t.Id !== soilTestId)
	}

	const archiveSoilTest = soilTestId => {
		const soilTest = allSoilTests.value.find(t => t.Id === soilTestId)
		if (!soilTest) return
		soilTest.Archived = true
		soilTest.DateArchived = +new Date()
	}

	const addRandomTest = () => {
		const id = uid()
		addNewSoilTest({
			Id: id,
			Name: `Test ${id.substring(0, 8)}`,
			Description: `Test ${id}`,
		})
	}

	const showAddDialog = () => {
		Dialog.create({
			component: AddSoilTestDialog,
			// componentProps: { successCallback },
		})
	}

	return {
		soilTests,
		latestTests,
		allSoilTests,
		archivedSoilTests,
		addNewSoilTest,
		deleteSoilTest,
		archiveSoilTest,
		addRandomTest,
		showAddDialog,
	}
}
