const routes = [
	{
		path: '/',
		component: () => import('pages/main/MainLayout.vue'),
		children: [
			{
				path: '',
				name: 'home',
				component: () => import('pages/main/IndexPage.vue'),
			},
			{
				name: 'overview',
				path: 'overview',
				component: () => import('pages/main/OverviewPage.vue'),
			},
			{
				name: 'faq',
				path: 'faq',
				component: () => import('pages/main/FaqPage.vue'),
			},
			{
				name: 'pricing',
				path: 'pricing',
				component: () => import('pages/main/PricingPage.vue'),
			},
			{
				name: 'register',
				path: 'register',
				component: () => import('pages/main/RegisterPage.vue'),
			},
			{
				name: 'login',
				path: 'login',
				component: () => import('pages/main/LoginPage.vue'),
			},
		],
	},
	{
		path: '/admin',
		component: () => import('pages/admin/AdminLayout.vue'),
		children: [
			{
				path: '',
				name: 'admin',
				component: () => import('pages/admin/IndexPage.vue'),
			},
			{
				path: 'tests/archived',
				name: 'archived-tests',
				component: () => import('pages/admin/ArchivedTestsPage.vue'),
			},
			// make sure this one is last so that others get matched first
			{
				path: 'tests/:id',
				name: 'view-test',
				props: true,
				component: () => import('pages/admin/SoilTestDetailPage.vue'),
			},
		],
	},

	// Always leave this as last one,
	// but you can also remove it
	{
		path: '/:catchAll(.*)*',
		component: () => import('pages/ErrorNotFound.vue'),
	},
]

export default routes
